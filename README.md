# Template for a thesis in the [Group for Digitalized Energy Systems](https://uol.de/des)

## Prequists
The template was tested with TexStudio and TexLive.

## Informationen
A detailed description of the template can be found in [mainThesis.pdf](https://gitlab.uni-oldenburg.de/des/vorlage/abschlussarbeit/-/blob/master/mainThesis.pdf)

## License
This template is licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) by UOL-DES.

